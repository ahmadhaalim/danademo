import demo.EmployeeRequest;
import demo.EmployeeResponse;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.theories.suppliers.TestedOn;


public class Employee {
 @Test
    public void getEmployee(){
     Response response = RestAssured
             .given()
             .baseUri("http://dummy.restapiexample.com")
             .basePath("/api")
             .log()
             .all()
             .header("Content-type", "application/json")
             .get("/v1/employees");
     response.getBody().prettyPrint();
     System.out.println(response.getStatusCode());
     Assert.assertEquals(200,response.getStatusCode());
     Assert.assertThat("Lama cuy",response.getTime(), Matchers.lessThan(3000L));
     Assert.assertEquals("success",response.path("status"));
     Assert.assertEquals("Tiger Nixon",response.path("data[0].employee_name"));
     EmployeeResponse employeeResponse = response.as(EmployeeResponse.class);
  System.out.println(employeeResponse.getStatus());


 }

 @Test
    public void createEmploye(){
  EmployeeRequest employeeRequest = new EmployeeRequest();
  employeeRequest.setName("Halim");
  employeeRequest.setAge("23");
  employeeRequest.setSalary("20000");
//  String requestBody = "{\n" +
//             "  \"name\": \"Dana\",\n" +
//             "  \"salary\": \"123\",\n" +
//             "  \"age\": \"23\"\n" +
//             "}";
     Response response = RestAssured
             .given()
             .baseUri("http://dummy.restapiexample.com")
             .basePath("/api")
             .log()
             .all()
             .header("Content-type", "application/json")
             .body(employeeRequest)
             .post("v1/create");

     response.getBody().prettyPrint();

 }
}
